import Image1 from '../images/react.png'
import Image2 from '../images/angular.png'
import Image3 from '../images/php.svg'
import Image4 from '../images/Vue.png'
import Image5 from '../images/Ruby.png'

const productdata = [
    { 
      id: 1,
      title:"React",
      description:"Une biblipthèque Javascript pour créer des interfaces utilisateurs",
      url: Image1,
      votes:80,
      submittedAvatarUrl: "images/avatars/beast.png",
      productImageUrl: "images/products/code.jpeg",
    },
    { 
      id: 2,
      title:"Angular",
      description:"Un Framework Javascript moins bien que React",
      url: Image2,
      votes:20,
      submittedAvatarUrl: "images/avatars/beast.png",
      productImageUrl: "images/products/code.jpeg"
    },
    { 
      id: 3,
      title:"PHP",
      description:"langage de ces morts",
      url: Image3,
      votes:5,
      submittedAvatarUrl: "images/avatars/beast.png",
      productImageUrl: "images/products/code.jpeg"
    },
    { 
      id: 4,
      title:"Vue",
      description:"Une biblipthèque Javascript",
      url: Image4,
      votes:22,
      submittedAvatarUrl: "images/avatars/beast.png",
      productImageUrl: "images/products/code.jpeg"
    },
    { 
      id: 5,
      title:"Ruby",
      description:"Un framework Web pour faire des prototypes rapides",
      url: Image5,
      votes:50,
      submittedAvatarUrl: "images/avatars/beast.png",
      productImageUrl: "images/products/code.jpeg"
    }
  ]
  
  export default productdata;