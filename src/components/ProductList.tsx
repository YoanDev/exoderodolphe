import React, { useState } from "react";
import productdata from "./data";
import Product from "./Product";

interface Props {}

export const ProductList = () => {
  const [products, setProducts] = useState(productdata);

  const handleProductUpVote = (productId: Number) => {
    const nexProducts = products.map((product) => {
      if (product.id === productId) {
        return Object.assign({}, product, { votes: product.votes + 1 });
      } else {
        return product;
      }
    });
    setProducts(nexProducts);
  };
  return (
    <>
      {products.map((product, k) => {
        return (
          <>
            <Product
              className="ui unstackable items"
              key={"product-" + product.id}
              id={product.id}
              title={product.title}
              description={product.description}
              url={product.url}
              votes={product.votes}
              submittedAvatarUrl={product.submittedAvatarUrl}
              productImageUrl={product.productImageUrl}
              onVote={() => handleProductUpVote(product.id)}
            />
          </>
        );
      })}
    </>
  );
};
