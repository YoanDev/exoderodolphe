import React from "react";

interface Props {
    productImageUrl: string,
    onVote: Function,
    id: Number,
    votes: Number,
    title: string,
    description: string,
    url: string,
    submittedAvatarUrl: string,
    className: string
}

const Product = (props: Props) => {
    const handleUpVote =()=>{
        props.onVote(props.id)
      }
  return (
    <div className={props.className}>
      <div className="item">
        <div className="image">
          <img src={props.productImageUrl} alt="code" />
        </div>
        <div className="middle aligned content">
          <div className="header">
            <a onClick={handleUpVote}>
              <i className="large caret up icon" />
            </a>
            {props.votes}
          </div>
          <div className="description">
            <a href={props.url}>{props.title}</a>
            <p>{props.description}</p>
          </div>
          <div className="extra">
            <span>Publié par: </span>
            <img
              src={props.submittedAvatarUrl}
              className="ui avatar image"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Product;
